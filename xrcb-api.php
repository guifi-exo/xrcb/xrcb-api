<?php
/*
Plugin Name: XRCB API
Description: Custom REST API for XRCB
Author: Gerald Kogler
Author URI: http://go.yuri.at
Text Domain: xrcbapi
*/

/**
 * load textdomain
 */
add_action( 'init', 'xrcbapi_load_textdomain' );
function xrcbapi_load_textdomain() {
	load_plugin_textdomain('xrcbapi', false, dirname(plugin_basename(__FILE__)));
}

/**
 * custom API endpoint for Radio Fabra and others
 * /wp-json/xrcb/v1/radios
 * /wp-json/xrcb/v1/radios/<radio_id>
 */
function xrcbapi_radios_api_endpoint( $request_data ) {

	// setup query argument
	$args = array(
		'post_type' => 'radio',
		'posts_per_page' => 100,
	);

	// limit to id
	if ($request_data->get_param('id')) {
		$args['p'] = $request_data->get_param('id');
	}

    //trigger_error(json_encode($args), E_USER_WARNING);

	// Run a custom query
    $meta_query = new WP_Query($args);

    //trigger_error(json_encode($meta_query->request), E_USER_WARNING);

	$posts = $meta_query->get_posts();

	// add custom field data to posts array	
	foreach ($posts as $key => $post) {
		$acf = get_fields($post->ID);

		$posts[$key]->acf = $acf;
		$posts[$key]->link = get_permalink($post->ID);
		$posts[$key]->rss_feed = site_url() . '/subscribirse-a-podcast/?id=' . $post->ID;

		//$posts[$key]->image = get_the_post_thumbnail_url($post->ID);
		//$posts[$key]->lat = (float)get_fields($post->ID)['location']['lat'];
		//$posts[$key]->lng = (float)get_fields($post->ID)['location']['lng'];
	}
	return $posts;
}

/**
 * custom API endpoint for Radio Fabra and others
 * /wp-json/xrcb/v1/podcasts
 * /wp-json/xrcb/v1/podcasts/<podcast_id>
 * /wp-json/xrcb/v1/podcasts?radio_id=<radio_id>
 * /wp-json/xrcb/v1/podcasts?radio_id=<radio_id>&format=application/rss+xml
 * /wp-json/xrcb/v1/podcasts?program_id=<program_id>
 * /wp-json/xrcb/v1/podcasts?radio_id=<radio_id>&program_id=<program_id>&format=application/rss+xml
 */
function xrcbapi_podcasts_api_endpoint( $request_data ) {

	// setup query arguments for posts and reposts
	$args = array(
		'post_type' => 'podcast',
		'posts_per_page' => 200,
	);
	$repost_args = array(
		'post_type' => 'repost',
		'posts_per_page' => 200,
	);

	// limit to id
	if ($request_data->get_param('id')) {

		$args['p'] = $request_data->get_param('id');
		$repost_args['p'] = $request_data->get_param('id');
	}

	// limit to radio
	if ($request_data->get_param('radio_id')) {

		$args['meta_key'] = 'radio';
		$args['meta_value'] = $request_data->get_param('radio_id');

		// get author id from radio
		$radio_args = array(
			'post_type' => 'radio',
			'posts_per_page' => 1,
			'p' => $request_data->get_param('radio_id')
		);
	    $radio_query = new WP_Query($radio_args);
		$radio = $radio_query->get_posts();

		$repost_args['author'] = $radio[0]->post_author;
	}

	// limit to program
	if ($request_data->get_param('program_id')) {

		$args['tax_query'] = array(
			array(
				'taxonomy' 	=> 'podcast_programa',
				'terms' 	=> $request_data->get_param('program_id'),
			),
		);
		$repost_args['tax_query'] = array(
			array(
				'taxonomy' 	=> 'podcast_programa',
				'terms' 	=> $request_data->get_param('program_id'),
			),
		);
	}

    $meta_query = new WP_Query($args);
	$posts = $meta_query->get_posts();

	// get reposts and merge into posts
	$reposts_query = new WP_Query($repost_args);
	$reposts = $reposts_query->get_posts();
	$posts = array_merge($posts, $reposts);

	// add custom field data to posts array	
	foreach ($posts as $key => $post) {
		$posts[$key]->acf = get_fields($post->ID);
		$posts[$key]->link = get_permalink($post->ID);
	}

	if (isset($_GET['format']) && 
		esc_sql($_GET['format']) == 'application/rss+xml' &&
		$request_data->get_param('radio_id')) {

		$radio_id = $request_data->get_param('radio_id');

		$program_id = null;
		if ($request_data->get_param('program_id')) {
			$program_id = $request_data->get_param('program_id');
		}

		// return RSS feed for podcast apps
		//trigger_error(json_encode(getRssFeed($posts)), E_USER_WARNING);
		return getRssFeed($meta_query, $radio_id, $program_id);
	}

	return $posts;
}

/**
 * output response as XML RSS Feed
 */
function getRssFeed($posts, $radio_id, $program_id=null) {

	$radio = get_post( $radio_id );

	$rssfeed = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;
    $rssfeed .= '<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">'.PHP_EOL;
    $rssfeed .= '<channel>'.PHP_EOL;
    $rssfeed .= '<title>XRCB Radio feed - '.$radio->post_title.'</title>'.PHP_EOL;
    $rssfeed .= '<link>'.get_permalink( $radio_id ).'</link>'.PHP_EOL;
    $rssfeed .= '<description>XRCB</description>'.PHP_EOL;
	$rssfeed .= '<author>XRCB - '.$radio->post_title.'</author>'.PHP_EOL;
    $rssfeed .= '<language>' . get_post_meta(get_the_ID(), 'idioma_podcast', true) . '</language>'.PHP_EOL;
    $rssfeed .= '<copyright>Copyleft (CC) 2023 xrcb.cat</copyright>'.PHP_EOL;

    $rssfeed .= '<itunes:subtitle>streaming community radios</itunes:subtitle>'.PHP_EOL;
	$rssfeed .= '<itunes:author>XRCB - '.$radio->post_title.'</itunes:author>'.PHP_EOL;
	$rssfeed .= '<itunes:summary></itunes:summary>'.PHP_EOL;
	$rssfeed .= '<itunes:owner>'.PHP_EOL;
	$rssfeed .= '<itunes:name>XRCB</itunes:name>'.PHP_EOL;
	$rssfeed .= '<itunes:email>info@xrcb.cat</itunes:email>'.PHP_EOL;
	$rssfeed .= '</itunes:owner>'.PHP_EOL;

    $img = wp_get_attachment_url(get_post_meta($radio_id, 'img_podcast', true));
    if ($img == false)
        $img = "https://xrcb.cat/wp-content/themes/xrcb/images/Logo-podcasts.jpg";
	$rssfeed .= '<itunes:image href="' . $img . '" />'.PHP_EOL;
	$rssfeed .= '<itunes:category text="Culture">'.PHP_EOL;
	$rssfeed .= '<itunes:category text=""/>'.PHP_EOL;
	$rssfeed .= '<itunes:category text=""/>'.PHP_EOL;
	$rssfeed .= '</itunes:category>'.PHP_EOL;
	$rssfeed .= '<itunes:category text="">'.PHP_EOL;
	$rssfeed .= '<itunes:category text=""/>'.PHP_EOL;
	$rssfeed .= '</itunes:category>'.PHP_EOL;
	$rssfeed .= '<itunes:explicit>clean</itunes:explicit>'.PHP_EOL;
	$rssfeed .= '<itunes:keywords></itunes:keywords>'.PHP_EOL.PHP_EOL;

	// add item for each post
	while($posts->have_posts()) {
		$posts->the_post();

		$rssfeed .= '<item>'.PHP_EOL;
        $rssfeed .= '<title>' . get_the_title() . '</title>'.PHP_EOL;
        $rssfeed .= '<itunes:summary>' . htmlspecialchars(get_the_content()) . '</itunes:summary>'.PHP_EOL;
        $rssfeed .= '<description>' . htmlspecialchars(get_the_content()) . '</description>'.PHP_EOL;

        $mp3_meta = wp_get_attachment_metadata(get_post_meta(get_the_ID(), 'file_mp3', true));
        $mp3_meta_filesize = "";
        if (isset($mp3_meta['filesize'])) $mp3_meta_filesize = $mp3_meta['filesize'];
        $mp3_meta_mime_type = "";
        if (isset($mp3_meta['mime_type'])) $mp3_meta_mime_type = $mp3_meta['mime_type'];
        $rssfeed .= '<enclosure url="' . wp_get_attachment_url(get_post_meta(get_the_ID(), 'file_mp3', true)) . '" length="' . $mp3_meta_filesize . '" type="' . $mp3_meta_mime_type . '" />'.PHP_EOL;

        $rssfeed .= '<guid isPermaLink="false">' . get_the_guid() . '</guid>'.PHP_EOL;
        $rssfeed .= '<link>' . get_the_permalink() . '</link>'.PHP_EOL;
        $rssfeed .= '<pubDate>' . get_post_time(DATE_RSS, false, get_the_ID(), false) . '</pubDate>'.PHP_EOL;
        $rssfeed .= '</item>'.PHP_EOL.PHP_EOL;
    }

    $rssfeed .= '</channel>'.PHP_EOL;
    $rssfeed .= '</rss>'.PHP_EOL;

	//return $rssfeed;	

	//header("Content-Type: application/rss+xml; charset=UTF-8");

	echo $rssfeed;
}

/**
 * custom API endpoint for broadcasts
 * /wp-json/xrcb/v1/broadcast
 */
function xrcbapi_broadcast_api_endpoint( $request_data ) {

	global $wpdb;

	// clear cache -> now done by nginx
	//$wpdb->flush();

	$params = array();

	$query_playing = $wpdb->prepare("SELECT id,date,start,end
		FROM wp_ea_appointments
		WHERE date = CURDATE() AND start < CURTIME() AND end > CURTIME()",
		$params
	);
	$playing = $wpdb->get_results($query_playing);

	if (count($playing) > 0) {
		$playing = $playing[0];

		// get podcast id
		$query_podcast = $wpdb->prepare("SELECT value
			FROM wp_ea_fields
			WHERE app_id=$playing->id AND field_id=5",
			$params
		);
		$podcast_id = (int)$wpdb->get_results($query_podcast)[0]->value;

		if ($podcast_id) {

			// get podcast data
			$playing->title = get_post($podcast_id)->post_title;
			$playing->description = get_post($podcast_id)->post_content;
			$playing->radio_id = get_post_meta($podcast_id, 'radio', true);
			$playing->radio_name = get_the_title(get_post_meta($podcast_id, 'radio', true));
			$playing->radio_link = get_permalink(get_post_meta($podcast_id, 'radio', true));
			$playing->link = get_permalink($podcast_id);
			$playing->live = get_post_meta($podcast_id, 'live', true)=="true";

		}
		else {
			// get live data

			// title field
			$query_title = $wpdb->prepare("SELECT value
				FROM wp_ea_fields
				WHERE app_id=$playing->id AND field_id=11",
				$params
			);
			$title = $wpdb->get_results($query_title)[0]->value;

			$playing->title = $title;
			$playing->live = true;

			$playing->file = false;
			$playing->radio = false;
			$playing->radiolink = false;
			$playing->podcastlink = false;
			$playing->algorithm = false;
		}

		echo json_encode(array("data" => $playing));
	}
	else {
		echo json_encode(array("data" => null));
	}
}

/**
 * custom API endpoint for broadcasts
 * /wp-json/xrcb/v1/broadcast
 */
function xrcbapi_liveevents_api_endpoint( $request_data ) {

	global $wpdb;

	$params = array();

	$query_events = $wpdb->prepare("SELECT id,date,start,end
		FROM wp_ea_appointments
		WHERE (date = CURDATE() AND end > CURTIME()) OR date > CURDATE()",
		$params
	);
	$events = $wpdb->get_results($query_events);

	$liveevents = array();
	foreach ($events as $event) {

		// get podcast id
		$query_podcast = $wpdb->prepare("SELECT value
			FROM wp_ea_fields
			WHERE app_id=$event->id AND field_id=5",
			$params
		);
		$podcast_id = (int)$wpdb->get_results($query_podcast)[0]->value;

		if ($podcast_id) {
			if (get_post_meta($podcast_id, 'live', true)=="true") {
				// get podcast data
				$event->title = get_post($podcast_id)->post_title;
				$event->description = get_post($podcast_id)->post_content;
				$event->radio_id = get_post_meta($podcast_id, 'radio', true);
				$event->radio_name = get_the_title(get_post_meta($podcast_id, 'radio', true));
				$event->radio_link = get_permalink(get_post_meta($podcast_id, 'radio', true));
				$event->podcast_link = get_permalink($podcast_id);
				$event->live = true;

				$liveevents[] = $event;
			}
		}
		else {
			// get live data
			$query_title = $wpdb->prepare("SELECT value
				FROM wp_ea_fields
				WHERE app_id=$event->id AND field_id=11",
				$params
			);
			$title = $wpdb->get_results($query_title)[0]->value;

			$event->title = $title;
			$event->live = true;

			$event->radio = false;
			$event->radio_link = false;
			$event->podcast_link = false;

			// get image url
			$query_title = $wpdb->prepare("SELECT value
				FROM wp_ea_fields
				WHERE app_id=$event->id AND field_id=17",
				$params
			);
			$image_url = $wpdb->get_results($query_title)[0]->value;
			if (!$image_url) $image_url = "";
			
			$event->image_url = $image_url;

			$liveevents[] = $event;
		}
	}
	echo json_encode(array("data" => $liveevents));
}

/**
 * register the endpoints
 */
add_action( 'rest_api_init', function () {

	register_rest_route( 'xrcb/v1', '/radios', array(
		'methods' => 'GET',
		'summary' => 'Get radios.',
		'description' => 'Get a list of radios.',
		'produces' => array(
			'application/json',
			'application/vnd.geo+json'
		),
		'callback' => 'xrcbapi_radios_api_endpoint',
	));

	register_rest_route( 'xrcb/v1', '/radios/(?P<id>\d+)', array(
		'methods' => 'GET',
		'summary' => 'Get radio by ID.',
		'description' => 'Get a single radio.',
		'produces' => array(
			'application/json',
			'application/vnd.geo+json'
		),
		'callback' => 'xrcbapi_radios_api_endpoint',
		'args' => array(
			'id' => array(
				'description' => esc_html__( 'Unique identifier for the object.', 'xrcb' ),
				'type'        => 'integer',
				'prop_format' => 'int64',
		        'validate_callback' => function($param, $request, $key) {
		        	return is_numeric( $param );
		        },
		    ),
		),
	));

	register_rest_route( 'xrcb/v1', '/podcasts', array(
		'methods' => 'GET',
		'summary' => 'Get podcasts.',
		'description' => 'Get a list of podcasts.',
		'produces' => array(
			'application/json',
			'application/vnd.geo+json'
		),
		'callback' => 'xrcbapi_podcasts_api_endpoint',
		'args' => array(
			'program_id' => array(
				'description' => esc_html__( 'Unique identifier for the program.', 'xrcb' ),
				'type'        => 'integer',
				'prop_format' => 'int64',
		        'validate_callback' => function($param, $request, $key) {
		        	return is_numeric( $param );
		        }
		    ),
			'radio_id' => array(
				'description' => esc_html__( 'Unique identifier for the radio.', 'xrcb' ),
				'type'        => 'integer',
				'prop_format' => 'int64',
		        'validate_callback' => function($param, $request, $key) {
		        	return is_numeric( $param );
		        }
		    ),
			'format' => array(
				'description' => esc_html__( 'application/json or application/rss+xml, default: application/json.', 'xrcb' ),
				'type'        => 'string',
		    ),
		),

	));

	register_rest_route( 'xrcb/v1', '/podcasts/(?P<id>\d+)', array(
		'methods' => 'GET',
		'summary' => 'Get podcast by ID.',
		'description' => 'Get a single podcast.',
		'produces' => array(
			'application/json',
			'application/vnd.geo+json'
		),
		'callback' => 'xrcbapi_podcasts_api_endpoint',
		'args' => array(
			'id' => array(
				'description' => esc_html__( 'Unique identifier for the object.', 'xrcb' ),
				'type'        => 'integer',
				'prop_format' => 'int64',
		        'validate_callback' => function($param, $request, $key) {
		        	return is_numeric( $param );
		        }
		    ),
		),
	));

	register_rest_route( 'xrcb/v1', '/broadcast', array(
		'methods' => 'GET',
		'summary' => 'Get broadcast info.',
		'description' => 'Gets information about the actual playing podcast from icecast stream main.mp3 at https://icecast.xrcb.cat/ Retrieves information from calendar at https://xrcb.cat/ca/calendari/.',
		'produces' => array(
			'application/json'
		),
		'callback' => 'xrcbapi_broadcast_api_endpoint',
	));

	register_rest_route( 'xrcb/v1', '/broadcast/liveevents', array(
		'methods' => 'GET',
		'summary' => 'Get next live broadcasted events.',
		'description' => 'Gets information about the upcoming live events.',
		'produces' => array(
			'application/json'
		),
		'callback' => 'xrcbapi_liveevents_api_endpoint',
	));
});

?>