# XRCB API Wordpress Plugin

Wordpress REST API plugin for https://xrcb.cat/

## Custom API endpoints 

### Retrieve last (100) custom posts *podcast*

#### Definition

`GET /xrcb/v1/podcasts`

#### Example Requests

`$ curl https://example.com/wp-json/xrcb/v1/podcasts/`

### Retrieve one *podcast*

#### Definition

`GET /xrcb/v1/podcasts/<id>`

#### Arguments

| id | post id of custom post *podcast*

#### Example Requests

`$ curl https://example.com/wp-json/xrcb/v1/podcasts/99`

### Query *podcast* by radio_id

#### Definition

`GET /xrcb/v1/podcasts?radio_id=<radio_id>`

#### Arguments

| radio_id | post id of custom post *radio*

#### Example Requests

`$ curl /xrcb/v1/podcasts?radio_id=100`

### Query *podcast* by program_id

#### Definition

`GET /xrcb/v1/podcasts?program_id=<program_id>`

#### Arguments

| program_id | taxonomy id of *podcast* custom taxonomy *podcast_programa*

#### Example Requests

`$ curl /xrcb/v1/podcasts?program_id=101`

### Retrieve last (100) custom posts *radio*

#### Definition

`GET /xrcb/v1/radios`

#### Example Requests

`$ curl https://example.com/wp-json/xrcb/v1/radios/`

### Retrieve one *radio*

#### Definition

`GET /xrcb/v1/radios/<id>`

#### Arguments

| id | post id of custom post *radio*

#### Example Requests

`$ curl https://example.com/wp-json/xrcb/v1/radios/99`
